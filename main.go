package main
import (
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"gitlab.com/shaikhkamran/gameofcards/games"
)

func main() {
	r := mux.NewRouter()
	r = games.AllRotes(r)
	log.Fatal(http.ListenAndServe(":8080", r));
}
