package games

import (
	"github.com/gorilla/mux"
)

func AllRotes(r *mux.Router) *mux.Router {
    r.HandleFunc("/getallgames", GetAllGames)
	r.HandleFunc("/getgame", GetGames)
	return r
}